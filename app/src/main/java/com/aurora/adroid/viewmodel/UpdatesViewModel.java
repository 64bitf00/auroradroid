/*
 * Aurora Droid
 * Copyright (C) 2019-20, Rahul Kumar Patel <whyorean@gmail.com>
 *
 * Aurora Droid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Aurora Droid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Aurora Droid.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.aurora.adroid.viewmodel;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.aurora.adroid.database.AppRepository;
import com.aurora.adroid.database.PackageRepository;
import com.aurora.adroid.model.App;
import com.aurora.adroid.model.Package;
import com.aurora.adroid.model.items.UpdatesItem;
import com.aurora.adroid.util.CertUtil;
import com.aurora.adroid.util.PackageUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class UpdatesViewModel extends BaseViewModel {

    private AppRepository appRepository;
    private PackageRepository packageRepository;
    private MutableLiveData<List<UpdatesItem>> data = new MutableLiveData<>();
    private PackageManager packageManager;

    public UpdatesViewModel(@NonNull Application application) {
        super(application);
        this.appRepository = new AppRepository(application);
        this.packageRepository = new PackageRepository(application);
        this.packageManager = application.getPackageManager();
        fetchUpdatableApps();
    }

    public MutableLiveData<List<UpdatesItem>> getAppsLiveData() {
        return data;
    }

    public void fetchUpdatableApps() {
        disposable.add(Observable.fromCallable(() -> getInstalledPackages())
                .subscribeOn(Schedulers.io())
                .map(packages -> {
                    final List<App> appList = new ArrayList<>();

                    for (String packageName : packages) {
                        final List<App> repoAppList = appRepository.getAppsByPackageName(packageName);
                        for (App app : repoAppList) {
                            //Get all packages associated with this app, in specific repo.
                            final List<Package> pkgList = packageRepository.getAllPackages(packageName, app.getRepoName());
                            //Get installed app signer
                            final String RSA256 = CertUtil.getSHA256(getApplication(), app.getPackageName());

                            if (!pkgList.isEmpty()) {
                                //Find best matching app package for the app
                                app.setPackageList(pkgList, RSA256, true);
                            }

                            final Package pkg = app.getAppPackage();
                            final PackageInfo packageInfo = PackageUtil.getPackageInfo(packageManager, app.getPackageName());

                            if (pkg != null && packageInfo != null) {
                                if (pkg.getVersionCode() > packageInfo.versionCode
                                        && (PackageUtil.isCompatibleVersion(getApplication(), pkg, packageInfo))
                                        && RSA256.equals(pkg.getSigner())) {
                                    app.setAppPackage(pkg);
                                    appList.add(app);
                                }
                            }
                        }
                    }
                    return appList;
                })
                .map(this::sortList)
                .flatMap(apps -> Observable
                        .fromIterable(apps)
                        .map(UpdatesItem::new))
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(updatesItems -> data.setValue(updatesItems), Throwable::printStackTrace));
    }
}
