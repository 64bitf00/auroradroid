• Added new repositories Bitwarden, LibRetro
• Improve repo sync.
• Fixed download issue on Android 10
• Bug fixes and improvements
